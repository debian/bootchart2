bootchart2 (0.14.4-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/control:
    + Bump Standards-Version to 4.4.0.
    + Update Vcs-* fields to use git package repo under Salsa Debian
      group.
    + Use secure URI in homepage field.
  * debian/patches: Add patch 0006 from upstream to fix FTBFS against
    glibc 2.28. (Closes: #916026)
  * debian/watch: Correctly monitor GitHub upstream.
  * debian/copyright:
    + Use secure URI.
    - Delete fields for missing files.
  * debian/bootchart2.lintian-overrides: Add lintian override for
    init.d-script-depends-on-all-virtual-facility (suppress warning
    for now).

 -- Boyuan Yang <byang@debian.org>  Wed, 11 Sep 2019 10:19:53 -0400

bootchart2 (0.14.4-3) unstable; urgency=low

  * Added missing dependency on lsb-base (>= 3.0-6). Thanks to Adam D.
    Barratt for noticing this!

 -- David Paleino <dapal@debian.org>  Wed, 12 Dec 2012 21:25:44 +0100

bootchart2 (0.14.4-2) unstable; urgency=low

  * Acknowledge NMU.
  * Provide also an initscript (Closes: #694403)

 -- David Paleino <dapal@debian.org>  Sat, 08 Dec 2012 19:35:08 +0100

bootchart2 (0.14.4-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Apply patches supplied by upstream to fix RC bug (Closes: #642343)
    - 0001-pybootchartgui-fix-parsing-of-non-ascii-bytes-in-log.patch
    - 0002-pybootchartgui-skip-malformed-taskstats-lines.patch
    - 0003-pybootchartgui-ensure-a-MemSample-is-valid-before-st.patch
    - 0004-pybootchartgui-be-more-tolerant-when-parsing-proc_me.patch

 -- Jonathan McCrohan <jmccrohan@gmail.com>  Sat, 08 Sep 2012 13:54:28 +0100

bootchart2 (0.14.4-1) unstable; urgency=low

  * New upstream version
    - added openbox and xfce4 to EXIT_PROC (Closes: #671869, #670659)
    - added support for initramfs (Closes: #603656)
  * Updated debian/copyright
  * Standards-Version bump to 3.9.3, no changes needed

 -- David Paleino <dapal@debian.org>  Thu, 07 Jun 2012 23:25:10 +0200

bootchart2 (0.14.2-1) unstable; urgency=low

  * New upstream version
  * Use linux-any as architecture for bootchart2

 -- David Paleino <dapal@debian.org>  Sun, 16 Oct 2011 17:46:54 +0200

bootchart2 (0.14.1-1) unstable; urgency=low

  * New upstream version
    - fixed AttributeError on Trace instance (Closes: #633894)
    - use correct extension when saving pybootchartgui's output
      (Closes: #631905)
    - document available output formats for pybootchartgui
      (Closes: #631904)
  * Added missing runtime dependencies to pybootchartgui
    (Closes: #633070)
  * Patches merged upstream, removed
  * Disabled build-time testing (fails somehow)
  * Standards-Version bumped to 3.9.2, no changes needed
  * Package converted to dh_python2

 -- David Paleino <dapal@debian.org>  Wed, 07 Sep 2011 09:37:58 +0200

bootchart2 (0.14.0-3) unstable; urgency=low

  * Bump minimum Python version to 2.6 (Closes: #619360)

 -- David Paleino <dapal@debian.org>  Tue, 29 Mar 2011 11:19:30 +0200

bootchart2 (0.14.0-2) unstable; urgency=low

  * Fix installed files in pybootchartgui (Closes: #619165)

 -- David Paleino <dapal@debian.org>  Mon, 21 Mar 2011 20:19:40 +0100

bootchart2 (0.14.0-1) unstable; urgency=low

  * New upstream version
    - manpages added, thanks to Francesca Ciceri (Closes: #611472)
  * Fix auto-reference in manpage (01-fix_manpage.patch)
  * Added Riccardo as co-maintainer (he's upstream too)

 -- David Paleino <dapal@debian.org>  Mon, 21 Mar 2011 11:05:11 +0100

bootchart2 (0.12.6-1) unstable; urgency=low

  * New upstream version
  * Upstream code switched from GPL-3+ to GPL-2+, fix this in
    debian/copyright, and do the same for debian/*
  * Riccardo Magliocchetti has been added to the list of AUTHORS.
    Congratulations, xrmx! :)
  * 00-fix_alternative_init.patch merged upstream, removed

 -- David Paleino <dapal@debian.org>  Sun, 26 Dec 2010 12:32:18 +0100

bootchart2 (0.12.4-2) unstable; urgency=low

  * Fix cmdline parsing for alternative init system, thanks to
    Riccardo Magliocchetti for the patch (Closes: #602684)

 -- David Paleino <dapal@debian.org>  Thu, 11 Nov 2010 22:22:59 +0100

bootchart2 (0.12.4-1) unstable; urgency=low

  * Initial release (Closes: #590858)

 -- David Paleino <dapal@debian.org>  Sun, 19 Sep 2010 19:07:42 +0200
